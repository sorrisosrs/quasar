/*
 * Informações gerais sobre unidades de construções do jogo.
 */
Quasar.game = {
	builds : {
		"main" : {
			"max" : 30,
			"name" : "Edifício principal",
			"wood" : 90,
			"stone" : 80,
			"iron" : 70,
			"pop" : 5,
			"fwood" : 1.26,
			"fstone" : 1.275,
			"firon" : 1.26,
			"fpop" : 1.17
		},
		"barracks" : {
			"max" : 25,
			"name" : "Quartel",
			"wood" : 200,
			"stone" : 170,
			"iron" : 90,
			"pop" : 7,
			"fwood" : 1.26,
			"fstone" : 1.28,
			"firon" : 1.26,
			"fpop" : 1.17
		},
		"stable" : {
			"max" : 20,
			"name" : "Estabulo",
			"wood" : 270,
			"stone" : 240,
			"iron" : 260,
			"pop" : 8,
			"fwood" : 1.26,
			"fstone" : 1.28,
			"firon" : 1.26,
			"fpop" : 1.17
		},
		"garage" : {
			"max" : 15,
			"name" : "Oficina",
			"wood" : 300,
			"stone" : 240,
			"iron" : 260,
			"pop" : 8,
			"fwood" : 1.26,
			"fstone" : 1.28,
			"firon" : 1.26,
			"fpop" : 1.17
		},
		"church" : {
			"max" : 3,
			"name" : "Igreja",
			"wood" : 16000,
			"stone" : 20000,
			"iron" : 5000,
			"pop" : 5000,
			"fwood" : 1.26,
			"fstone" : 1.28,
			"firon" : 1.26,
			"fpop" : 1.55
		},
		"church_f" : {
			"max" : 1,
			"name" : "Primeira Igreja",
			"wood" : 160,
			"stone" : 200,
			"iron" : 50,
			"pop" : 5,
			"fwood" : 1.26,
			"fstone" : 1.28,
			"firon" : 1.26,
			"fpop" : 1.55
		},
		"snob" : {
			"max" : 1,
			"name" : "Academia",
			"wood" : 15000,
			"stone" : 25000,
			"iron" : 10000,
			"pop" : 80,
			"fwood" : 2,
			"fstone" : 2,
			"firon" : 2,
			"fpop" : 1.17
		},
		"smith" : {
			"max" : 20,
			"name" : "Ferreiro",
			"wood" : 220,
			"stone" : 180,
			"iron" : 240,
			"pop" : 20,
			"fwood" : 1.26,
			"fstone" : 1.275,
			"firon" : 1.26,
			"fpop" : 1.17
		},
		"place" : {
			"max" : 1,
			"name" : "Praça",
			"wood" : 10,
			"stone" : 40,
			"iron" : 30,
			"pop" : 0,
			"fwood" : 1.26,
			"fstone" : 1.275,
			"firon" : 1.26,
			"fpop" : 1.17
		},
		"statue" : {
			"max" : 1,
			"name" : "Estabulo",
			"wood" : 220,
			"stone" : 220,
			"iron" : 220,
			"pop" : 10,
			"fwood" : 1.26,
			"fstone" : 1.275,
			"firon" : 1.26,
			"fpop" : 1.17
		},
		"market" : {
			"max" : 25,
			"name" : "Mercado",
			"wood" : 100,
			"stone" : 100,
			"iron" : 100,
			"pop" : 20,
			"fwood" : 1.26,
			"fstone" : 1.275,
			"firon" : 1.26,
			"fpop" : 1.17
		},
		"wood" : {
			"max" : 30,
			"name" : "Bosque",
			"wood" : 50,
			"stone" : 60,
			"iron" : 40,
			"pop" : 5,
			"fwood" : 1.25,
			"fstone" : 1.275,
			"firon" : 1.245,
			"fpop" : 1.155
		},
		"stone" : {
			"max" : 30,
			"name" : "Poço de argila",
			"wood" : 65,
			"stone" : 50,
			"iron" : 40,
			"pop" : 5,
			"fwood" : 1.27,
			"fstone" : 1.265,
			"firon" : 1.24,
			"fpop" : 1.14
		},
		"iron" : {
			"max" : 30,
			"name" : "Mina de ferro",
			"wood" : 75,
			"stone" : 65,
			"iron" : 70,
			"pop" : 10,
			"fwood" : 1.252,
			"fstone" : 1.275,
			"firon" : 1.24,
			"fpop" : 1.17
		},
		"farm" : {
			"max" : 30,
			"name" : "Fazenda",
			"wood" : 45,
			"stone" : 40,
			"iron" : 30,
			"pop" : 0,
			"fwood" : 1.3,
			"fstone" : 1.32,
			"firon" : 1.29,
			"fpop" : 1
		},
		"storage" : {
			"max" : 30,
			"name" : "Armazem",
			"wood" : 60,
			"stone" : 50,
			"iron" : 40,
			"pop" : 0,
			"fwood" : 1.265,
			"fstone" : 1.27,
			"firon" : 1.245,
			"fpop" : 1.15
		},
		"hide" : {
			"max" : 10,
			"name" : "Esconderijo",
			"wood" : 50,
			"stone" : 60,
			"iron" : 50,
			"pop" : 2,
			"fwood" : 1.25,
			"fstone" : 1.25,
			"firon" : 1.25,
			"fpop" : 1.17
		},
		"wall" : {
			"max" : 20,
			"name" : "Muralha",
			"wood" : 50,
			"stone" : 100,
			"iron" : 20,
			"pop" : 5,
			"fwood" : 1.26,
			"fstone" : 1.275,
			"firon" : 1.26,
			"fpop" : 1.17
		},
	},
	units : {
		"spear" : {
			"wood" : 50,
			"stone" : 30,
			"iron" : 10,
			"pop" : 1,
			"time" : 1020,
			"ed" : "barracks",
			"name" : "spear"
		},
		"sword" : {
			"wood" : 30,
			"stone" : 30,
			"iron" : 70,
			"pop" : 1,
			"time" : 1500,
			"ed" : "barracks",
			"name" : "sword"
		},
		"axe" : {
			"wood" : 60,
			"stone" : 30,
			"iron" : 40,
			"pop" : 1,
			"time" : 1320,
			"ed" : "barracks",
			"name" : "axe"
		},
		"archer" : {
			"wood" : 100,
			"stone" : 30,
			"iron" : 60,
			"pop" : 1,
			"time" : 1800,
			"ed" : "barracks",
			"name" : "archer"
		},
		"spy" : {
			"wood" : 50,
			"stone" : 50,
			"iron" : 20,
			"pop" : 2,
			"time" : 900,
			"ed" : "stable",
			"name" : "spy"
		},
		"light" : {
			"wood" : 100,
			"stone" : 125,
			"iron" : 250,
			"pop" : 4,
			"time" : 1800,
			"ed" : "stable",
			"name" : "light"
		},
		"marcher" : {
			"wood" : 250,
			"stone" : 100,
			"iron" : 150,
			"pop" : 5,
			"time" : 2700,
			"ed" : "stable",
			"name" : "marcher"
		},
		"heavy" : {
			"wood" : 200,
			"stone" : 150,
			"iron" : 600,
			"pop" : 6,
			"time" : 3600,
			"ed" : "stable",
			"name" : "heavy"
		},
		"ram" : {
			"wood" : 300,
			"stone" : 200,
			"iron" : 200,
			"pop" : 5,
			"time" : 4800,
			"ed" : "garage",
			"name" : "ram"
		},
		"catapult" : {
			"wood" : 320,
			"stone" : 400,
			"iron" : 100,
			"pop" : 8,
			"time" : 7200,
			"ed" : "garage",
			"name" : "catapult"
		}
	}
};